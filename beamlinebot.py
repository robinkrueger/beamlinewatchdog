"""
Telegram-Bot for monitoring synchrotron scans.
Author: Robin Krüger, robin.kruger@med.lu.se

Bot is programmed reagarding the two following pages
https://www.shellhacks.com/telegram-api-send-message-personal-notification-bot/
https://www.shellhacks.com/python-send-message-to-telegram/

# How to setup the Beamline Bot.

## Create New Bot & get api token (if you want to create another bot)

- Go to BotFather Chat
- /newbot
- choose name & username
- you will get a message with the API token

## Add user (and add chat IDs)
- search for the bot name (BeamlineUpdate) in your telegram app 
- click 'start'
- search for the bot name (BeamlineWarning) in your telegram app 
- click 'start'
- now you have to find your chatID:
  - open the 'Telegram Bot Raw' (Search for it invthe Telegram App)
  - click 'start'
  - extract your chatID from the answer ("chat"->"id")
- add your chatID to the python script (new entry into the dictionary) 
"""

import requests

apiToken_update = '5981589031:AAFPJ7rJv9otpzVmDGHYj3eMEL6zWH0HrAQ'
apiToken_warning = '6289751566:AAFFItI95VV-XuDV6hpv2izB4mV5lpZNugU'

# --- START CHANGE HERE --- #
chatIDdb = {"Robin": '0123456789',
           "Rosalind": '0123456789',
           "Lise": '0123456789',
           "Marie":'0123456789'}
# --- STOP CHANGE HERE --- #

def send_message(message,apiToken,chatID,printing=False):
    apiURL = f'https://api.telegram.org/bot{apiToken}/sendMessage'
    try:
        response = requests.post(apiURL, json={'chat_id': chatID, 'text': message})
        if printing:
            print(response.text)
        return response.ok
    except Exception as e:
        print(e)

def send_photo(photo_path, caption, apiToken, chatID, printing=False):
    apiURL = f'https://api.telegram.org/bot{apiToken}/sendPhoto'
    try:
        with open(photo_path, 'rb') as photo:
            response = requests.post(apiURL, data={'chat_id': chatID, 'caption': caption}, files={'photo': photo})
        if printing:
            print(response.text)
        return response.ok
    except Exception as e:
        print(e)
        
def send_update(people, message, path = None):
    for person in people:
        if path == None:
            response = send_message(message, apiToken_update, chatIDdb[person])
        else:
            response = send_photo(path, message, apiToken_update, chatIDdb[person])
        if response:
            print('Message was send succesfully to',person+'.')
        else:
            print('Failed to send Message to',person+'.')
            
def send_warning(people, message, path = None):
    for person in people:
        if path == None:
            response = send_message(message, apiToken_warning, chatIDdb[person])
        else:
            response = send_photo(path, message, apiToken_warning, chatIDdb[person])
        if response:
            print('Message was send succesfully to',person+'.')
        else:
            print('Failed to send Message to',person+'.')

#%% Example Usage
#photo_path = r"/home/robin/PhD/Lab/MSFbot/scatterin_direcitions.png"
#send_update(['Robin'],'The Telegram-Bot can now send images as well!',path=photo_path)




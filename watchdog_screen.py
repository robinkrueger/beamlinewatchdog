# -*- coding: utf-8 -*-
"""
Telegram-Bot for monitoring Synchrotron scans with having a webcam on the
screen of the beamline control computer.

Author: Robin Krüger, robin.kruger@med.lu.se

This script analysis the difference between two with the webcam aquired images and sends a warning (BeamlineWarning),
if the difference is not large enough, so it is assumable that the scan got stuck.

Addinal to that a message for each check is send if chosen to a second channel (BeamlineUpdate).
This channel can be set silent in Telegram.

To setup the chat connections go to beamlinebot.py.

If you get an error message please remember to install iamgeio-ffmpeg
pip install imageio-ffmpeg

Tested on Linux (Ubuntu).
"""
import time
import imageio as iio
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from beamlinebot import send_update, send_warning

# You have test out different video sources to find the right camera
video_source = "<video4>"

def to_grayscale(img):
    return np.dot(img[...,:3], [0.299, 0.587, 0.114])

def shot():
    camera = iio.get_reader(video_source)
    screenshot = camera.get_data(0)
    camera.close()
    return to_grayscale(screenshot)

def shot_RGB():
    camera = iio.get_reader(video_source)
    screenshot = camera.get_data(0)
    camera.close()
    return screenshot

#%% Check for right video input and decide for threshold value. You have tro try different video_sources to find the right number.

img1 = shot()
time.sleep(1)
img2 = shot()
diff = np.mean(np.abs(img2-img1))

fig, ax = plt.subplots(1,3)
ax[0].imshow(img1)
ax[0].set_title('Image 1')
ax[1].imshow(img2)
ax[1].set_title('Image 2')
ax[2].imshow(np.abs(img2-img1))
ax[2].set_title('Difference Image')

print('Deviation between images is: ', diff)

#%%

plt.close()

#%%

threshold = 5             # set thresold between follwoing images (test it out)
send_update_image = True  # decide if for each test an image is send to the BeamlineUpdate
sleep_time = 1            # time between checks in min 
path_img = r"tmp.png"     # path to temporary store an image
people = ['Robin']        # list of people to send notifications to (specify chatID in beamlinebot.py)

#%%

# inititalise images
img_last = shot()
img_curr = shot() 

# Start the checking loop
while True:
    # acquire new image
    img_last = img_curr
    img_curr_RGB = shot_RGB()
    img_curr = to_grayscale(img_curr_RGB)
    
    # calculate difference
    diff = np.mean(np.abs(img_last-img_curr))
    
    # send update for each chek if chosen
    if send_update_image == True:
        im = Image.fromarray(img_curr_RGB)
        im.save(path_img)
        send_update(people, 'Difference: '+str(diff), path = path_img)
    
    # check if image chageis below the threshold
    if diff < threshold:
        print('The sceen is not longer changing.')
        im = Image.fromarray(img_curr_RGB)
        im.save(path_img)
        send_warning(people, 'The sceen is not longer changing. Difference: '+str(diff), path = path_img)
    print('Checking Value:', diff)
    
    time.sleep(sleep_time*60)    






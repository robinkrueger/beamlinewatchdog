# -*- coding: utf-8 -*-
"""
Telegram-Bot for monitoring Synchrotron scans at the ForMAX beamline.

Author: Robin Krüger, robin.kruger@med.lu.se

This script analysis the directory of the azimutal integrated scattering data and sends a warning (BeamlineWarning),
if the last scan
- can not be opened
- has counts below a certain threshold
- was finished to long ago.

Addinal to that a message for each scan is send to a second channel (BeamlineUpdate) with the number of summed counts 
on the detector for each scan. This channel can be set silent in Telegram.

To setup the chat connections go to beamlinebot.py.
A possible kernel at the MaxIV cluster is "ForMAX / Tomography".
"""
import h5py
import time
import os
import numpy as np
from beamlinebot import send_update, send_warning

def get_intensity(fname):
    """get summed intensity from an azint fill"""
    with h5py.File(fname, 'r') as fh:
        data = np.sum(np.array(fh.get('entry/data1d/I')))
    return data

# --- START CHANGE ME --- #
persons = ['Lise']       # List of persons to send an update (add chatIDs therefore in beamlinebot.py)
threshold = 100          # Minimum integrated intensity to not send a warning
time_threshold = 2       # Maximum time between scans in min.
sleep_time = 10          # Test intervall in s
proposal = 20240042      # Proposal number to create the filepath
visit = 2024021408       # Visit number to create the filepath
path_to_files = "/data/visitors/formax/{}/{}/process/azint".format(proposal, visit) # Path to the monitored directory
# --- STOP CHANGE ME --- #

last_scan = ''
last_scan_time_s = time.time()
time_warning_send = False

# Start of test loop
while True:
    # Pick a scan file
    filenames = os.listdir(path_to_files)
    filenames.sort()
    newest_scan = filenames[-2]
    # The newest scan is not taken to not interefere with writing of the experimental data.
    
    if newest_scan != last_scan:
        # Get timestamp from the new scan
        newest_scan_time_s = os.stat(os.path.join(path_to_files,newest_scan)).st_mtime
        newest_scan_time_struct = time.gmtime(int(newest_scan_time_s))
        newest_scan_time_readable = time.strftime('%Y-%m-%d_%H-%M-%S',newest_scan_time_struct)
        
        # Get intensity from the new scan
        try:
            I = get_intensity(os.path.join(path_to_files,newest_scan))
            if I < threshold:
                send_warning(persons,'Scan {} shows intensity below threshold (I = {})'.format(newest_scan,I))
        except:
            send_warning(persons, 'Azint from Scan {} could not be opened.'.format(newest_scan))
            I = None
            
        # Send update message
        send_update(persons,'{}: Scan {} completed! I =  {}'.format(newest_scan_time_readable,newest_scan,I))
        last_scan = newest_scan
        last_scan_time_s = newest_scan_time_s
        time_warning_send = False
        
    # Send message send if the last scan was finisched to long ago.
    if (time.time()>last_scan_time_s+60*time_threshold) and not time_warning_send:
        send_warning(persons,'Last scan finished more than {} min. ago.'.format(time_threshold))
        time_warning_send = True
    time.sleep(sleep_time)
# BeamlineWatchdog

## Description

The BeamlineWatchdog is responsible for monitoring that a scan at a large scale facility is still running and to send out notifications via [Telegram](https://web.telegram.org/) if that is not the case. Since the beamline control computers are often not connected to the internet, the watchdogs have to run on other computers. Two attempts are made here so far:
- running the watchdog on a cluster which can access the file system with newly created scan files
- having a webcam filming the screen of the control computer.
## Installation
The beamline watchdog itself needs just the standard *requests* library in python. Therefore a standard installation of python is sufficient.

For the specific use-cases additional libraries are necessary:
- ForMAX
	- h5py
- Screen
	- imageio-ffmpeg

On the phones of the recipients the Telegram app has to be installed. The users need to have a username in Telegram, start the Bots (BeamlineUpdate https://t.me/beamline_bot and BeamlineWarning https://t.me/beamline_warning_bot) and find out their chatID.

#### Setup Telegram
- search for the bot name (BeamlineUpdate) in your telegram app 
- click *start*
- search for the bot name (BeamlineWarning) in your telegram app 
- click *start*
- now you have to find your chatID:
	- make sure you have chosen an username
- open the *Telegram Bot Raw* (Search for it in the Telegram App)
- click *start*
- extract your chatID from the answer ("chat"->"id")
- add your chatID to the python script *beamlinebot.py* (new entry into the dictionary)
## Contributing

If you would like to contribute interfaces for more beam-lines, please contact Robin.
## Logo

The logo for this gitlab repository was created by Copilot.


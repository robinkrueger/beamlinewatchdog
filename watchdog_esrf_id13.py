# -*- coding: utf-8 -*-
"""
Telegram-Bot for monitoring Synchrotron scans at the ESRF DI13 tensor-tomo experiments.

adpated for ESRF ID13 by Mads Carlsen

This script analysis the directory of the azimutal integrated scattering data and sends a warning (BeamlineWarning),
if the last scan
- can not be opened
- has counts below a certain threshold
- was finished to long ago.

Addinal to that a message for each scan is send to a second channel (BeamlineUpdate) with the number of summed counts 
on the detector for each scan. This channel can be set silent in Telegram.

To setup the chat connections go to beamlinebot.py.
"""
import h5py
import time
import os
import numpy as np
from beamlinebot import send_update, send_warning

# --- START CHANGE ME --- #
sample_name = 'parakeet_nose' # This should be changed every time the sample is changed
persons = ['Torne', 'Carolina']       # List of persons to send an update (add chatIDs therefore in beamlinebot.py)
time_threshold = 15       # Maximum time between scans in min.
warning_frequency_min = 5 # How often warnings are send when no new files are detected.
sleep_time = 10          # Test intervall in s
proposal = 'ab1234'      # Proposal number to create the filepath
session = '20240420'       # Visit number to create the filepath
path_to_files = f"/data/visitor/{proposal}/id13/{session}/RAW_DATA/{sample_name}" # Path to the monitored directory
# --- STOP CHANGE ME --- #

last_scan = ''
last_scan_time_s = time.time()
last_warning_send = -np.inf

if __name__ == "__main__":
    
    subfolders_before = os.listdir(path_to_files)
    subfolders_before.sort()
    last_scan = subfolders_before[-1]
    
    while True:
        
        # Check if there are any new folders
        subfolders_now = os.listdir(path_to_files)
        new_subfolders = list(set(subfolders_now) - set(subfolders_before))
        
        if len(new_subfolders) == 0: # If no new folders
            time_since_last_scan_s = time.time() - last_scan_time_s
            
            if time_since_last_scan_s < time_threshold * 60: # Time below critical
                print('all good going to sleep')
                pass
            elif time_since_last_scan_s >= time_threshold * 60: # Time above critical
            
                time_since_last_warning = time.time() - last_warning_send
                
                if time_since_last_warning > warning_frequency_min * 60:
                    send_warning(persons, f"It has been {int(time_since_last_scan_s//60)} minutes and {int(time_since_last_scan_s%60)} seconds since the last scan!")
                    last_warning_send = time.time()
                    print(f'Trying to wake up {persons[0]}')
        elif len(new_subfolders) > 0: # If new folders
            new_subfolders.sort()
            send_update(persons, f"Scan folder named {new_subfolders[-1]} has recently been created.")
            last_scan = new_subfolders[-1]
            last_scan_time_s = time.time()
            subfolders_before = subfolders_now
            print('all good going to sleep')
        
        time.sleep(sleep_time)
 
